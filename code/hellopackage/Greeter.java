package hellopackage;
import java.util.Scanner;
import java.util.Random;

import secondpackage.Utilities;

public class Greeter{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter a value");
        int value = scan.nextInt();

        int answer = Utilities.doubleMe(value);
        System.out.println(answer);
    }
}